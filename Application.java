public class Application
{
	public static void main(String[] args)
	{
		Student student1 = new Student();
		Student student2 = new Student();
		Student student3 = new Student();
		
		student1.age = 17;
		student1.semester = 2;
		student1.gpa = 3.1;
		
		student2.age = 18;
		student2.semester = 3;
		student2.gpa = 2.5;
		
		student3.age = 20;
		student3.semester = 6;
		student3.gpa = 3.0;
		
		student1.studyMaterial();
		student2.studyMaterial();
		
		student1.passExams();
		student2.passExams();
		
		Student[] section3 = new Student[3];
		
		section3[0] = student1;
		section3[1] = student2;
		section3[2] = student3;
		
		System.out.println(section3[2].gpa);
	}
}